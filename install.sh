printf "Installing dependencies..."
yay -S river foot wofi i3status-rust i3bar-river-git bind geoip geoip-database
printf "Copying configs..."
cp config/.config/* /home/$USER/.config/
printf "Authentication may be required to install display manager session"
sudo cp config/River /usr/local/share/wayland-sessions/
printf "Copying scripts"
cp scripts/* /home/$USER/
